
/**
 * BookingServiceCallbackHandler.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis2 version: 1.5.1  Built on : Oct 19, 2009 (10:59:00 EDT)
 */

    package soap.trains.spyne;

    /**
     *  BookingServiceCallbackHandler Callback class, Users can extend this class and implement
     *  their own receiveResult and receiveError methods.
     */
    public abstract class BookingServiceCallbackHandler{



    protected Object clientData;

    /**
    * User can pass in any object that needs to be accessed once the NonBlocking
    * Web service call is finished and appropriate method of this CallBack is called.
    * @param clientData Object mechanism by which the user can pass in user data
    * that will be avilable at the time this callback is called.
    */
    public BookingServiceCallbackHandler(Object clientData){
        this.clientData = clientData;
    }

    /**
    * Please use this constructor if you don't want to set any clientData
    */
    public BookingServiceCallbackHandler(){
        this.clientData = null;
    }

    /**
     * Get the client data
     */

     public Object getClientData() {
        return clientData;
     }

        
           /**
            * auto generated Axis2 call back method for getAllClasses method
            * override this method for handling normal response from getAllClasses operation
            */
           public void receiveResultgetAllClasses(
                    soap.trains.spyne.BookingServiceStub.GetAllClassesResponseE result
                        ) {
           }

          /**
           * auto generated Axis2 Error handler
           * override this method for handling error response from getAllClasses operation
           */
            public void receiveErrorgetAllClasses(java.lang.Exception e) {
            }
                
           /**
            * auto generated Axis2 call back method for bookTravel method
            * override this method for handling normal response from bookTravel operation
            */
           public void receiveResultbookTravel(
                    soap.trains.spyne.BookingServiceStub.BookTravelResponseE result
                        ) {
           }

          /**
           * auto generated Axis2 Error handler
           * override this method for handling error response from bookTravel operation
           */
            public void receiveErrorbookTravel(java.lang.Exception e) {
            }
                
           /**
            * auto generated Axis2 call back method for getAvailablesTrains method
            * override this method for handling normal response from getAvailablesTrains operation
            */
           public void receiveResultgetAvailablesTrains(
                    soap.trains.spyne.BookingServiceStub.GetAvailablesTrainsResponseE result
                        ) {
           }

          /**
           * auto generated Axis2 Error handler
           * override this method for handling error response from getAvailablesTrains operation
           */
            public void receiveErrorgetAvailablesTrains(java.lang.Exception e) {
            }
                
           /**
            * auto generated Axis2 call back method for getAllStations method
            * override this method for handling normal response from getAllStations operation
            */
           public void receiveResultgetAllStations(
                    soap.trains.spyne.BookingServiceStub.GetAllStationsResponseE result
                        ) {
           }

          /**
           * auto generated Axis2 Error handler
           * override this method for handling error response from getAllStations operation
           */
            public void receiveErrorgetAllStations(java.lang.Exception e) {
            }
                
           /**
            * auto generated Axis2 call back method for login method
            * override this method for handling normal response from login operation
            */
           public void receiveResultlogin(
                    soap.trains.spyne.BookingServiceStub.LoginResponseE result
                        ) {
           }

          /**
           * auto generated Axis2 Error handler
           * override this method for handling error response from login operation
           */
            public void receiveErrorlogin(java.lang.Exception e) {
            }
                
           /**
            * auto generated Axis2 call back method for register method
            * override this method for handling normal response from register operation
            */
           public void receiveResultregister(
                    soap.trains.spyne.BookingServiceStub.RegisterResponseE result
                        ) {
           }

          /**
           * auto generated Axis2 Error handler
           * override this method for handling error response from register operation
           */
            public void receiveErrorregister(java.lang.Exception e) {
            }
                


    }
    