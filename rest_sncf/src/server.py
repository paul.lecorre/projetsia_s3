from typing import Union
from fastapi import FastAPI, HTTPException
import psycopg2 as pg
from datetime import datetime
from os import environ

app = FastAPI()

def get_connection():
    return pg.connect(host=environ["SNCFDB_HOSTNAME"],
            user=environ["SNCFDB_USER"],
            password=environ["SNCFDB_PASSWORD"],
            port=int(environ["SNCFDB_PORT"]),
            dbname=environ["SNCFDB_DATABASE"])

def rs_to_dict(cursor):
    colnames = [desc[0] for desc in cursor.description]

    # Récupération des résultats
    rows = cursor.fetchall()

    # Conversion des résultats en liste de dictionnaires
    return [dict(zip(colnames, row)) for row in rows]

@app.get("/stations")
def get_stations():
    db = get_connection()
    cursor = db.cursor()
    cursor.execute("SELECT * FROM public.stations")
    data = rs_to_dict(cursor)
    cursor.close()
    db.close()
    return {"stations": list(data)}

@app.get("/classes")
def get_classes():
    db = get_connection()
    cursor = db.cursor()
    cursor.execute("SELECT class_id, name FROM public.classes")
    data = rs_to_dict(cursor)
    cursor.close()
    db.close()
    return {"classes": list(data)}

@app.get("/travels")
def get_travels(from_station: str, to_station: str, class_name: str, from_date: str = datetime.now().strftime("%Y-%m-%dT%H:%M:%S"), to_date: str = "9999-12-31T00:00:00", nb_seats: int = 1):
    
    try:
        from_date = int(datetime.strptime(from_date, "%Y-%m-%dT%H:%M:%S").timestamp())
        to_date = int(datetime.strptime(to_date, "%Y-%m-%dT%H:%M:%S").timestamp())
    except ValueError:
        raise HTTPException(status_code=400, detail="Invalid date format")    
    
    db = get_connection()
    cursor = db.cursor()
    columns = """t.travel_id, t.departure_date, t.arrival_date, t.from_station_id, sf.name as from_station_name, sf.city as from_station_city, 
                 t.to_station_id, st.name as to_station_name, st.city as to_station_city, tr.train_id, tr.train_code, c.class_id, c.name as class_name, rst.nb_seats as remaining_seats"""
                         
    cursor.execute(f"""SELECT {columns}
                    FROM public.travels AS t
                    JOIN public.trains AS tr ON (t.train_id = tr.train_id)
                    JOIN public.stations AS sf ON (t.from_station_id = sf.station_id)
                    JOIN public.stations AS st ON (t.to_station_id = st.station_id)
                    JOIN public.remaining_seats_travel AS rst ON (t.travel_id = rst.travel_id)
                    JOIN public.classes AS c ON (rst.class_id = c.class_id)
                    WHERE sf.name = %s AND st.name = %s 
                    AND t.departure_date BETWEEN to_timestamp(%s) AND to_timestamp(%s) AND c.name = %s""", (from_station, to_station, from_date, to_date, class_name))
    
    data = rs_to_dict(cursor)
    def f(x):
        x['departure_date'] = datetime.fromtimestamp(x['departure_date']).strftime("%Y-%m-%dT%H:%M:%S")
        x['arrival_date'] = datetime.fromtimestamp(x['arrival_date']).strftime("%Y-%m-%dT%H:%M:%S")
        return x
    map(f, data)
    cursor.close()
    db.close()
    return {"travels": list(data)}

@app.patch("/travels/{travel_id}")
def book_travel(travel_id : int, class_name: str, nb_seats: int = 1):
    db = get_connection()
    cursor = db.cursor()
    cursor.execute("""SELECT rst.class_id, rst.nb_seats FROM public.remaining_seats_travel AS rst JOIN public.classes AS c ON (rst.class_id = c.class_id)
                   WHERE rst.travel_id = %s AND c.name = %s""", (travel_id, class_name))
    data = rs_to_dict(cursor)[0]
    if len(data) == 0:
        db.rollback()
        db.close()
        raise HTTPException(status_code=404, detail="Travel id not found")
    if data['nb_seats'] >= nb_seats:
        class_id = data['class_id']
        cursor.execute("""UPDATE public.remaining_seats_travel SET nb_seats = nb_seats - %s WHERE travel_id = %s AND class_id = %s""", (nb_seats, travel_id, class_id))
        db.commit()
        db.close()
        return {"success": "travel booked"}
    else:
        db.rollback()
        db.close()
        raise HTTPException(status_code=403, detail="Travel fully booked")
