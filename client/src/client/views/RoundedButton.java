package client.views;

// Classe pour les boutons arrondis

import java.awt.Graphics;

import javax.swing.JButton;

class RoundedButton extends JButton {
    RoundedButton(String label) {
        super(label);
        setContentAreaFilled(false);
    }

    protected void paintComponent(Graphics g) {
        g.setColor(getBackground());
        g.fillRoundRect(0, 0, getSize().width - 1, getSize().height - 1, 25, 25);
        super.paintComponent(g);
    }

    protected void paintBorder(Graphics g) {
        g.setColor(getForeground());
        g.drawRoundRect(0, 0, getSize().width - 1, getSize().height - 1, 25, 25);
    }
}
