package client.controlers;

import java.awt.GridLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.math.BigInteger;
import java.text.SimpleDateFormat;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.Date;
import java.util.LinkedList;
import java.util.List;

import javax.swing.JComboBox;
import javax.swing.JFrame;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JPasswordField;
import javax.swing.JRadioButton;
import javax.swing.JSpinner;
import javax.swing.JTextField;

import client.views.TrainPanel;
import soap.trains.spyne.BookingServiceStub;
import soap.trains.spyne.BookingServiceStub.*;

public class ResearchListener implements ActionListener {
    JComboBox gare1;
    JComboBox gare2;
    JComboBox classe;
    JSpinner startDateField;
    JSpinner returnDateField;
    JTextField nbTicket;
    JFrame frame;
    JPanel centerPanel;
    JRadioButton roundTrip;
    String token;

    private Object[][] getTravels(String date, JComboBox gare1, JComboBox gare2) {
        Object[][] data = null;
        try {
            BookingServiceStub stub = null;

            try {
                stub = new BookingServiceStub();
            } catch (org.apache.axis2.AxisFault e) {
                System.err.println(e.toString());
            }
            
            TravelParams params = new TravelParams();
            params.setFrom_station(String.valueOf(gare1.getSelectedItem()));
            params.setTo_station(String.valueOf(gare2.getSelectedItem()));
            params.setOutbound(date);
            params.setNb_seats(new BigInteger(nbTicket.getText()));
            params.setSeat_class(String.valueOf(classe.getSelectedItem()));
            GetAvailablesTrainsE params2 = new GetAvailablesTrainsE();
            GetAvailablesTrains trains = new GetAvailablesTrains();
            trains.setParams(params);
            trains.setToken(token);
            params2.setGetAvailablesTrains(trains);
            GetAvailablesTrainsResponseE resp = stub.getAvailablesTrains(params2);
            TravelArray listTrains = resp.getGetAvailablesTrainsResponse().getGetAvailablesTrainsResult();

            // Supposons que listTrains soit votre liste de trains
            Travel[] travels = listTrains.getTravel();

            // Initialisation de la taille du tableau en fonction du nombre de trains
            data = new Object[travels.length][6];

            SimpleDateFormat format1 = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
            // Remplissage du tableau à partir des informations des trains
            for (int i = 0; i < travels.length; i++) {
                Travel train = travels[i];
                data[i][0] = train.getTravel_id();
                data[i][1] = train.getTrain_code();
                data[i][2] = format1.format(train.getDeparture_date().getTime());
                data[i][3] = format1.format(train.getArrival_date().getTime());
                data[i][4] = train.getCompany();
                data[i][5] = train.getRemaining_seats();
            }


        } catch (Exception e) {
            System.err.println(e.toString());
        }

        return data;

    }

    public ResearchListener(JComboBox gare1, JComboBox gare2, JComboBox classe, JSpinner startDateField,
            JSpinner returnDateField, JTextField nbTicket, JFrame frame, JPanel centerPanel, JRadioButton roundTrip,
            String token) {
        this.gare1 = gare1;
        this.gare2 = gare2;
        this.classe = classe;
        this.startDateField = startDateField;
        this.returnDateField = returnDateField;
        this.nbTicket = nbTicket;
        this.frame = frame;
        this.centerPanel = centerPanel;
        this.roundTrip = roundTrip;
        this.token = token;
    }

    @Override
    public void actionPerformed(ActionEvent e) {
        try {
            int intValue = Integer.parseInt(nbTicket.getText());
        } catch (NumberFormatException err) {
            JOptionPane.showMessageDialog(frame, "Erreur dans la saisie du nombre de ticket");
        }
        if (String.valueOf(gare1.getSelectedItem()).length()>0 && String.valueOf(gare2.getSelectedItem()).length()>0 && !nbTicket.getText().isEmpty()){
            if (startDateField.getValue() instanceof Date) {
                Date date = (Date) startDateField.getValue();
                SimpleDateFormat format = new SimpleDateFormat("yyyy-MM-dd");
                String startDate = format.format(date);
                if (!roundTrip.isSelected()) {
                    centerPanel.removeAll();
                    centerPanel.add(new TrainPanel(getTravels(startDate,gare1,gare2),token,nbTicket,classe));
                } else {
                    if (returnDateField.getValue() instanceof Date) {
                        date = (Date) returnDateField.getValue();
                        String returnDate = format.format(date);
                        centerPanel.removeAll();
                        centerPanel.add(new TrainPanel(getTravels(startDate,gare1,gare2), getTravels(returnDate,gare2,gare1),token,nbTicket,classe));
                    } else {
                        JOptionPane.showMessageDialog(frame, "Erreur dans la saisie des dates");
                    }
                }
                centerPanel.invalidate();
                centerPanel.validate();
                centerPanel.repaint();
                frame.pack();
            } else {
                JOptionPane.showMessageDialog(frame, "Erreur dans la saisie des dates");
            }
        } else {
            JOptionPane.showMessageDialog(frame, "Erreur dans la saisie des données");
        }

    }

}