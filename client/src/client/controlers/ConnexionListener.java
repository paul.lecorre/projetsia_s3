package client.controlers;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.JFrame;
import javax.swing.JOptionPane;
import javax.swing.JPasswordField;
import javax.swing.JTextField;

import client.App;
import soap.trains.spyne.AuthenticationServiceStub;
import soap.trains.spyne.AuthenticationServiceStub.*;

public class ConnexionListener implements ActionListener {
    JTextField id;
    JPasswordField mdp;
    JFrame frame;
    String token;

    private boolean login(String username, String Password) {
        try {
            AuthenticationServiceStub stub = null;

            try {
                stub = new AuthenticationServiceStub();
            } catch (org.apache.axis2.AxisFault e) {
                return false;
            }

            Login params = new Login();
            params.setUsername(username);
            params.setPassword(Password);
            LoginE params2 = new LoginE();
            params2.setLogin(params);
            LoginResponseE resp = stub.login(params2);
            this.token = resp.getLoginResponse().getLoginResult();
            return true;

        } catch (Exception e) {
            return false;
        }
    }

    public ConnexionListener(JTextField usernameLogin, JPasswordField passwordLogin, JFrame frame) {
        this.id = usernameLogin;
        this.mdp = passwordLogin;
        this.frame = frame;
    }

    @Override
    public void actionPerformed(ActionEvent e) {
        if (id.getText().length() == 0 || String.valueOf(mdp.getPassword()).length() == 0) {
            JOptionPane.showMessageDialog(frame, "Veuillez remplir correctement les champs");
        } else {
            if (login(id.getText(), String.valueOf(mdp.getPassword()))) {
                App.move2mainWindows(token);
            } else {
                JOptionPane.showMessageDialog(frame, "Erreur de connexion");
            }

        }
    }

}
