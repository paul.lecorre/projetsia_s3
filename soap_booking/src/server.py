from spyne import Application, rpc, ServiceBase, ComplexModel, String, Integer, Boolean, Iterable, DateTime, Fault, String
from spyne.protocol.soap import Soap11
from spyne.model.fault import Fault
from spyne.server.wsgi import WsgiApplication
from wsgiref.simple_server import make_server
from hashlib import sha256
from bcrypt import hashpw, gensalt, checkpw
from os import environ
import MySQLdb as mysql
from datetime import datetime
import requests
import json
import secrets

apis = json.load(open(environ["API_LIST_FILE"], 'r'))['apis']


def get_connection():
    return mysql.connect(host=environ["CONNEXION_HOSTNAME"],
                         user=environ["CONNEXION_USER"],
                         password=environ["CONNEXION_PASSWORD"],
                         port=int(environ["CONNEXION_PORT"]),
                         database=environ["CONNEXION_DATABASE"])


def query(query, params):
    db = get_connection()
    cursor = db.cursor(mysql.cursors.DictCursor)
    cursor.execute(query, params)
    data = cursor.fetchall()
    db.commit()
    db.close()
    return data


def _login(username, password):
    rs = query(
        "SELECT user_id, password FROM accounts where username = %s", (username,))
    if rs:
        hashed_passwd = rs[0]['password']
        if checkpw(password.encode('utf-8'), hashed_passwd.encode('utf-8')):
            return rs[0]['user_id']

    return None


def check_token(token, ip_addr):
    token_hashed = sha256((token + ip_addr).encode('utf-8')).hexdigest()
    rs = query("SELECT user_id FROM tokens WHERE token = %s", (token_hashed,))
    if rs:
        return rs[0]['user_id']
    return None


def create_token(user_id, ip_addr):

    # Delete previous tokens
    query("DELETE FROM tokens WHERE user_id = %s", (user_id,))
    
    token = secrets.token_urlsafe(64)
    
    token_hashed = sha256((token + ip_addr).encode("UTF-8")).hexdigest()
    query("INSERT INTO tokens (token, user_id) VALUES (%s, %s)",
          (token_hashed, user_id))

    return token


def user_exists(username):
    return bool(query("SELECT user_id FROM accounts WHERE username = %s", (username,)))

def rest_merge_lists(f_url, list_key):
    result = []
    for api in apis:
        r = requests.get(f_url(api['url_base']))
        result += list(map(lambda d: {**d, 'company': api['name']}, r.json()[list_key]))
    return result


class AuthenticationError(Fault):
    """ Exception personnalisée pour les erreurs d'authentification. """
    pass


class TravelParams(ComplexModel):
    from_station = String(required=True)
    to_station = String(required=True)
    outbound = String(required=True)
    seat_class = String(required=True)
    nb_seats = Integer(default=1, required=False)
    
class Station(ComplexModel):
    name = String(required=True)
    city = String(required=True)

class Travel(ComplexModel):
    company = String(required=True)
    travel_id = Integer(required=True)
    departure_date = DateTime(required=True)
    arrival_date = DateTime(required=True)
    from_station = String(required=True)
    to_station = String(required=True)
    train_code = String(required=True)
    class_name = String(required=True)
    remaining_seats = Integer(required=True)

class BookingService(ServiceBase):
    @rpc(String, TravelParams, _returns=Iterable(Travel))
    def getAvailablesTrains(ctx, token, params):
        if not check_token(token, ctx.transport.req['REMOTE_ADDR']):
            return AuthenticationError("Incorrect token")
        
        params = params.as_dict()
        outbound = datetime.strptime(params['outbound'],"%Y-%m-%d")
        params['from_date'] = outbound.strftime("%Y-%m-%dT00:00:00")
        params['to_date'] = outbound.strftime("%Y-%m-%dT23:59:59")
        params['class_name'] = params['seat_class']
        del params['seat_class']
            
        url_params = "&".join(map(lambda k: k + "=" + str(params[k]), params.keys()))
        
        result = rest_merge_lists(lambda url: url + "travels" + "?" + url_params, "travels")
        
        l = []
        

        for r in result:
            t = Travel(company=r['company'], 
                       travel_id=r['travel_id'], 
                       departure_date=datetime.strptime(r['departure_date'],"%Y-%m-%dT%H:%M:%S"), 
                       arrival_date=datetime.strptime(r['arrival_date'],"%Y-%m-%dT%H:%M:%S"), 
                       from_station=r['from_station_name'], 
                       to_station=r['to_station_name'], 
                       train_code=r['train_code'], 
                       class_name=r['class_name'], 
                       remaining_seats=r['remaining_seats'])
            
            l.append(t)
        
        return l

    @rpc(String, _returns=Iterable(Station))
    def getAllStations(ctx, token):
        if not check_token(token, ctx.transport.req['REMOTE_ADDR']):
            raise AuthenticationError("Incorrect token")
        
        result = rest_merge_lists(lambda url: url + "stations", "stations")
        
        stations = list(set(map(lambda r: (r['name'], r['city']), result)))
        return [Station(name=s[0], city=s[1]) for s in stations]
             

    @rpc(String, _returns=Iterable(String))
    def getAllClasses(ctx, token):
        if not check_token(token, ctx.transport.req['REMOTE_ADDR']):
            raise AuthenticationError("Incorrect token")
        
        result = rest_merge_lists(lambda url: url + "classes", "classes")
        return list(set(r['name'] for r in result))

    @rpc(String, String, Integer, Integer, String, _returns=Boolean)
    def bookTravel(ctx, token, company, travel_id, nb_seats, seat_class):
        if not check_token(token, ctx.transport.req['REMOTE_ADDR']):
            raise AuthenticationError("Incorrect token")
        
        api = next(filter(lambda a: a['name'] == company, apis))
        
        url_params = "class_name=" + seat_class + "&nb_seats=" + str(nb_seats)
        
        r = requests.patch(api['url_base'] + "travels/" + str(travel_id) + "?" + url_params)
        
        return r.status_code == 200
        

class AuthenticationService(ServiceBase):
    @rpc(String, String, _returns=String)
    def login(ctx, username: str, password: str):
        user_id = _login(username, password)

        if user_id:
            token = create_token(user_id, ctx.transport.req['REMOTE_ADDR'])

            return token


        raise AuthenticationError("Authentication failed")

    @rpc(String, String, _returns=String)
    def register(ctx, username: str, password: str):

        if user_exists(username):
            raise AuthenticationError("User already exists")

        hashed_password = hashpw(password.encode('utf-8'), gensalt())

        query("INSERT INTO accounts (username, password) VALUES (%s, %s);",
              (username, hashed_password))

        return "User created"


app = Application([BookingService, AuthenticationService],
                  'spyne.trains.soap',
                  in_protocol=Soap11(validator='lxml'),
                  out_protocol=Soap11())

wsgi_app = WsgiApplication(app)

if __name__ == '__main__':
    # You can use any Wsgi server. Here, we chose
    # Python's built-in wsgi server but you're not
    # supposed to use it in production.
    from wsgiref.simple_server import make_server
    server = make_server('0.0.0.0', 8000, wsgi_app)
    server.serve_forever()
