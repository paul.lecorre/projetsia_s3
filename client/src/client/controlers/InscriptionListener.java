package client.controlers;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.JFrame;
import javax.swing.JOptionPane;
import javax.swing.JPasswordField;
import javax.swing.JTextField;
import soap.trains.spyne.AuthenticationServiceStub;
import soap.trains.spyne.AuthenticationServiceStub.*;

public class InscriptionListener implements ActionListener {
    JTextField id;
    JPasswordField mdp;
    JFrame frame;

    public InscriptionListener(JTextField usernameRegister, JPasswordField passwordRegister, JFrame frame) {
        this.id = usernameRegister;
        this.mdp = passwordRegister;
        this.frame = frame;
    }

    private String register(String username, String Password) {
        try {
            AuthenticationServiceStub stub = null;

            try {
                stub = new AuthenticationServiceStub();
            } catch (org.apache.axis2.AxisFault e) {
                return e.toString();
            }

            Register params = new Register();
            params.setUsername(username);
            params.setPassword(Password);
            RegisterE params2 = new RegisterE();
            params2.setRegister(params);
            RegisterResponseE resp = stub.register(params2);
            return resp.getRegisterResponse().getRegisterResult();

        } catch (Exception e) {
            return e.toString();
        }
    }

    @Override
    public void actionPerformed(ActionEvent e) {
        if (id.getText().length() == 0 || String.valueOf(mdp.getPassword()).length() == 0) {
            JOptionPane.showMessageDialog(frame, "Veuillez remplir correctement les champs");
        } else {
            JOptionPane.showMessageDialog(frame, register(id.getText(), String.valueOf(mdp.getPassword())));
        }
    }

}