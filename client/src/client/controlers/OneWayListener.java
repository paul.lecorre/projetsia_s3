package client.controlers;

import java.awt.event.*;

import javax.swing.JRadioButton;
import javax.swing.JSpinner;

public class OneWayListener implements ActionListener {
    JRadioButton roundTrip;
    JSpinner returnDateField;

    public OneWayListener(JRadioButton roundTrip, JSpinner returnDateField) {
        this.roundTrip = roundTrip;
        this.returnDateField = returnDateField;
    }

    @Override
    public void actionPerformed(ActionEvent e) {
        boolean selected = roundTrip.isSelected();
        returnDateField.setEnabled(selected);
    }

}