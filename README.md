BINOME: Pierre Barbey et Paul Le Corre

SOAP/REST/DB:
    besoins : docker
    run : docker compose up


JAVA CLIENT:
    besoins : jdk-19 que les services SOAP/REST soit lancé et sur linux
    run : source set-env.sh ; cd client && make run


OPEN API DOCS:
    besoins : SOAP/REST start
    docs rest sncf : http://localhost:8001/docs
    docs rest sncb : http://localhost:8002/docs
