package client.controlers;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.math.BigInteger;
import java.text.SimpleDateFormat;

import javax.swing.JComboBox;
import javax.swing.JOptionPane;
import javax.swing.JTable;
import javax.swing.JTextField;

import client.App;
import soap.trains.spyne.BookingServiceStub;
import soap.trains.spyne.BookingServiceStub.*;

public class ReservationListener implements ActionListener {
    JTable table1;
    JTable table2;
    String token;
    JComboBox classe;
    JTextField nbSeats;

    public Object GetData(JTable table, int row_index, int col_index) {
        return table.getModel().getValueAt(row_index, col_index);
    }

    private boolean reserve(JTable table, int selectRow) {
        boolean rc = false;
        try {

            BookingServiceStub stub = null;

            try {
                stub = new BookingServiceStub();
            } catch (org.apache.axis2.AxisFault e) {
                System.err.println(e.toString());
            }

            BookTravelE bookTravelE0 = new BookTravelE();
            BookTravel bookTravel0 = new BookTravel();
            bookTravel0.setTravel_id(new BigInteger(String.valueOf(GetData(table, selectRow, 0))));
            bookTravel0.setToken(token);
            bookTravel0.setSeat_class(String.valueOf(classe.getSelectedItem()));
            bookTravel0.setCompany(String.valueOf(GetData(table, selectRow, 4)));
            bookTravel0.setNb_seats(new BigInteger(nbSeats.getText()));
            bookTravelE0.setBookTravel(bookTravel0);

            BookTravelResponseE resp = stub.bookTravel(bookTravelE0);
            rc = resp.getBookTravelResponse().getBookTravelResult();
        } catch (Exception e) {
            System.err.println(e.toString());
        }

        return rc;
    }

    public ReservationListener(JTable table, String token, JComboBox classe, JTextField nbSeats) {
        this.table1 = table;
        this.table2 = null;
        this.token = token;
        this.classe = classe;
        this.nbSeats = nbSeats;
    }

    public ReservationListener(JTable trainTableLeft, JTable trainTableRight, String token, JComboBox classe,
            JTextField nbSeats) {
        this.table1 = trainTableLeft;
        this.table2 = trainTableRight;
        this.token = token;
        this.classe = classe;
        this.nbSeats = nbSeats;
    }

    @Override
    public void actionPerformed(ActionEvent e) {
        if (table2 == null) {
            if (table1.getSelectedRow() < 0) {
                JOptionPane.showMessageDialog(App.mainWindow, "Veuillez selectionner une ligne");
            } else {
                if(reserve(table1, table1.getSelectedRow())){
                    JOptionPane.showMessageDialog(App.mainWindow, "Reservation réussie");
                }else{
                    JOptionPane.showMessageDialog(App.mainWindow, "Erreur lors de la reservation");
                }
            }

        } else {
            if (table1.getSelectedRow() < 0 && table2.getSelectedRow() < 0) {
                JOptionPane.showMessageDialog(App.mainWindow, "Veuillez selectionner une ligne dans chaque tableau");
            } else {
                if(reserve(table1, table1.getSelectedRow()) && reserve(table2, table2.getSelectedRow())){
                    JOptionPane.showMessageDialog(App.mainWindow, "Reservation réussie");
                }else{
                    JOptionPane.showMessageDialog(App.mainWindow, "Erreur lors de la reservation");
                }
            }
        }
    }

}
