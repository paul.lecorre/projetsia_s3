package client.views;

import javax.swing.*;

import client.controlers.*;

import java.awt.*;

public class BookingWindow extends JFrame {

    private String token;

    public BookingWindow(String token) {
        this.token = token;
        setTitle("Réservation de Billets");
        setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        setSize(1920, 1080); // Ajusté à la taille suggérée par l'image
        setLayout(new BorderLayout(10, 10)); // Espacement pour la lisibilité

        // Panneau du haut pour les options Aller et Aller/Retour, les champs de texte
        // et les checkboxes
        JPanel northPanel = new JPanel(new GridLayout(5, 1, 5, 5)); // Avec GridLayout pour organiser les sous-panneaux

        // Panneau pour les options Aller et Aller/Retour
        JPanel typePanel = new JPanel(new FlowLayout());
        JRadioButton oneWay = new JRadioButton("Allé", true);
        JRadioButton roundTrip = new JRadioButton("Allé retour");
        ButtonGroup typeGroup = new ButtonGroup();
        typeGroup.add(oneWay);
        typeGroup.add(roundTrip);
        typePanel.add(oneWay);
        typePanel.add(roundTrip);

        // Panneau pour les champs de saisie
        JPanel fieldsPanel = new JPanel(new FlowLayout());
        fieldsPanel.add(new JLabel("From"));
        String[] optionsGare = GetAllScrolList.getStations(token);
        JComboBox<String> gare1 = new JComboBox<>(optionsGare);
        fieldsPanel.add(gare1);
        fieldsPanel.add(new JLabel("To"));
        JComboBox<String> gare2 = new JComboBox<>(optionsGare);
        fieldsPanel.add(gare2);
        fieldsPanel.add(new JLabel("Date départ"));
        SpinnerDateModel dateModel = new SpinnerDateModel();
        JSpinner startDateField = new JSpinner(dateModel);
        JSpinner.DateEditor dateEditor = new JSpinner.DateEditor(startDateField, "dd/MM/yyyy");
        startDateField.setEditor(dateEditor);
        fieldsPanel.add(startDateField);

        // Champ de texte pour la date de retour
        fieldsPanel.add(new JLabel("Date retour"));
        SpinnerDateModel dateModelReturn = new SpinnerDateModel();
        JSpinner returnDateField = new JSpinner(dateModelReturn);
        JSpinner.DateEditor dateEditorReturn = new JSpinner.DateEditor(returnDateField, "dd/MM/yyyy");
        returnDateField.setEditor(dateEditorReturn);
        fieldsPanel.add(returnDateField);
        returnDateField.setEnabled(false);

        fieldsPanel.add(new JLabel("Nb tickets"));
        JTextField nbTicket = new JTextField(3);
        fieldsPanel.add(nbTicket);

        // Panneau pour les checkboxes des classes
        JPanel classesPanel = new JPanel(new FlowLayout());
        classesPanel.add(new JLabel("Classes"));
        String[] options = GetAllScrolList.getClasses(token);

        JComboBox<String> classes = new JComboBox<>(options);
        classesPanel.add(classes);

        JPanel researchPanel = new JPanel(new FlowLayout());
        JButton rechercher = new JButton("Rechercher");
        researchPanel.add(rechercher);

        northPanel.add(typePanel);
        northPanel.add(fieldsPanel);
        northPanel.add(classesPanel);
        northPanel.add(researchPanel);
        northPanel.setBorder(BorderFactory.createTitledBorder(""));

        // Panneau central pour les options de train Allé et Retour
        JPanel centerPanel = new JPanel(new GridLayout(1, 2, 5, 5));
        centerPanel.add(new TrainPanel());

        oneWay.addActionListener(new OneWayListener(roundTrip, returnDateField));
        roundTrip.addActionListener(new OneWayListener(roundTrip, returnDateField));
        rechercher.addActionListener(new ResearchListener(gare1, gare2, classes, startDateField, returnDateField,
                nbTicket, this, centerPanel, roundTrip, token));

        // Ajouter tous les panneaux à la fenêtre principale
        add(northPanel, BorderLayout.NORTH);
        add(centerPanel, BorderLayout.CENTER);

        // Rendre la fenêtre visible
        pack(); // Ajuster la taille de la fenêtre aux composants
        setLocationRelativeTo(null); // Centrer la fenêtre
    }
}
