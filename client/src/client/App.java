package client;

import javax.swing.JFrame;

import client.views.*;

public class App {

    public static JFrame mainWindow;

    public static void move2mainWindows(String token) {
        mainWindow.setVisible(false);
        mainWindow = new BookingWindow(token);
        mainWindow.setVisible(true);
    }

    public static void main(String[] args) {
        mainWindow = new LoginRegister();
        mainWindow.setVisible(true);
    }

}
