from typing import Union
from fastapi import FastAPI, HTTPException
import MySQLdb as mysql
from datetime import datetime
from os import environ

app = FastAPI()

def get_connection():
    return mysql.connect(host=environ["SNCBDB_HOSTNAME"],
            user=environ["SNCBDB_USER"],
            password=environ["SNCBDB_PASSWORD"],
            port=int(environ["SNCBDB_PORT"]),
            database=environ["SNCBDB_DATABASE"])

@app.get("/stations")
def get_stations():
    db = get_connection()
    cursor = db.cursor(mysql.cursors.DictCursor)
    cursor.execute("SELECT * FROM stations")
    data = cursor.fetchall()
    cursor.close()
    db.close()
    return {"stations": list(data)}

@app.get("/classes")
def get_classes():
    db = get_connection()
    cursor = db.cursor(mysql.cursors.DictCursor)
    cursor.execute("SELECT class_id, name FROM classes")
    data = cursor.fetchall()
    cursor.close()
    db.close()
    return {"classes": list(data)}

@app.get("/travels")
def get_travels(from_station: str, to_station: str, class_name: str, from_date: str = datetime.now().strftime("%Y-%m-%dT%H:%M:%S"), to_date: str = "9999-12-31T00:00:00", nb_seats: int = 1):
    try:
        datetime.strptime(from_date, "%Y-%m-%dT%H:%M:%S")
        datetime.strptime(to_date, "%Y-%m-%dT%H:%M:%S")
    except ValueError:
        raise HTTPException(status_code=400, detail="Invalid date format")    
    
    db = get_connection()
    cursor = db.cursor(mysql.cursors.DictCursor)
    
    columns = """t.travel_id, t.departure_date, t.arrival_date, t.from_station_id, sf.name as from_station_name, sf.city as from_station_city, 
                 t.to_station_id, st.name as to_station_name, st.city as to_station_city, tr.train_id, tr.train_code, c.class_id, c.name as class_name, rst.nb_seats as remaining_seats"""
        
    cursor.execute(f"""SELECT {columns}
                    FROM travels AS t
                    JOIN trains AS tr ON (t.train_id = tr.train_id)
                    JOIN stations AS sf ON (t.from_station_id = sf.station_id)
                    JOIN stations AS st ON (t.to_station_id = st.station_id)
                    JOIN remaining_seats_travel AS rst ON (t.travel_id = rst.travel_id)
                    JOIN classes AS c ON (rst.class_id = c.class_id)
                    WHERE sf.name = %s AND st.name = %s 
                    AND t.departure_date BETWEEN %s AND %s AND c.name = %s""", (from_station, to_station, from_date, to_date, class_name))
    
    data = cursor.fetchall()
    cursor.close()
    db.close()
    return {"travels": list(data)}

@app.patch("/travels/{travel_id}")
def book_travel(travel_id : int, class_name: str, nb_seats: int = 1):
    db = get_connection()
    db.autocommit(False)
    cursor = db.cursor(mysql.cursors.DictCursor)
    cursor.execute("""SELECT rst.class_id, rst.nb_seats FROM remaining_seats_travel AS rst JOIN classes AS c ON (rst.class_id = c.class_id)
                   WHERE rst.travel_id = %s AND c.name = %s""", (travel_id, class_name))
    data = cursor.fetchone()
    if data is None:
        db.rollback()
        db.close()
        raise HTTPException(status_code=404, detail="Travel id not found")
    if data['nb_seats'] >= nb_seats:
        class_id = data['class_id']
        cursor.execute("UPDATE remaining_seats_travel SET nb_seats = nb_seats - %s WHERE travel_id = %s AND class_id = %s", (nb_seats, travel_id, class_id))
        db.commit()
        db.close()
        return {"success": "travel booked"}
    else:
        db.rollback()
        db.close()
        raise HTTPException(status_code=403, detail="Travel fully booked")