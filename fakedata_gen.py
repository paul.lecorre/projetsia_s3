#%%
import pandas as pd
import numpy as np
from datetime import datetime
from faker import Faker

"""
CREATE TABLE trains (
    train_id INTEGER AUTO_INCREMENT PRIMARY KEY,
    train_code varchar(4) not null,
    release_date DATETIME NOT NULL
);

CREATE TABLE classes (
    class_id INTEGER AUTO_INCREMENT PRIMARY KEY,
    name varchar(255) not null
);

CREATE TABLE stations (
    station_id INTEGER AUTO_INCREMENT PRIMARY KEY,
    city varchar(255) not null,
    name varchar(255) not null
);

CREATE TABLE travels (
    travel_id INTEGER AUTO_INCREMENT PRIMARY KEY,
    departure_date DATETIME NOT NULL,
    arrival_date DATETIME NOT NULL,
    from_station_id INTEGER NOT NULL,
    to_station_id INTEGER NOT NULL,
    train_id INTEGER NOT NULL,
    FOREIGN KEY (train_id) REFERENCES trains(train_id),
    FOREIGN KEY (from_station_id) REFERENCES stations(station_id),
    FOREIGN KEY (to_station_id) REFERENCES stations(station_id)
);

CREATE TABLE remaining_seats_travel (
    travel_id INTEGER NOT NULL,
    class_id INTEGER NOT NULL,
    nb_seats INTEGER DEFAULT 0,
    PRIMARY KEY (travel_id, class_id),
    FOREIGN KEY (class_id) REFERENCES classes(class_id),
    FOREIGN KEY (travel_id) REFERENCES travels(travel_id)
);

CREATE TABLE max_seats_train (
    train_id INTEGER NOT NULL,
    class_id INTEGER NOT NULL,
    nb_seats INTEGER DEFAULT 0,
    PRIMARY KEY (train_id, class_id),
    FOREIGN KEY (class_id) REFERENCES classes(class_id),
    FOREIGN KEY (train_id) REFERENCES trains(train_id)
);
"""

import random

fk = Faker()

# Fonctions pour générer des données pour chaque table
def gen_trains(n):
    for i in range(1, n+1):
        release_date = fk.date_time_between(start_date='-10y', end_date='now')
        release_date = datetime(release_date.year, release_date.month, release_date.day, release_date.hour, release_date.minute, release_date.second)
        yield {"train_id": i, "train_code": fk.bothify(text='????').upper(), "release_date":release_date.strftime('%Y-%m-%d %H:%M:%S')}

def gen_classes():
    yield {"class_id": 1, "name": 'First'}
    yield {"class_id": 2, "name": 'Business'}
    yield {"class_id": 3, "name": 'Standard'}

def gen_stations(n):
    for i in range(1, n+1):
        c = fk.city()
        yield { "station_id":i, "city": c, "name": c + " Station"}

def gen_travels(n, stations, trains):
    for i in range(1, n+1):
        from_id = stations.sample(1)['station_id'].values[0]
        to_id = stations.query(f"station_id != {from_id}").sample(1)['station_id'].values[0]
        departure_date = fk.date_time_between(start_date='now', end_date='+30d')
        arrival_date = departure_date + pd.Timedelta(hours=random.randint(1, 10))
        departure_date = datetime(departure_date.year, departure_date.month, departure_date.day, departure_date.hour, departure_date.minute, departure_date.second)
        arrival_date = datetime(arrival_date.year, arrival_date.month, arrival_date.day, arrival_date.hour, arrival_date.minute, arrival_date.second)
        yield { "travel_id" : i,
               "from_station_id": from_id,
               "to_station_id": to_id,
               "train_id": trains.sample(1)['train_id'].values[0],
               "departure_date": departure_date.strftime('%Y-%m-%d %H:%M:%S'),
               "arrival_date": arrival_date.strftime('%Y-%m-%d %H:%M:%S'),}


def gen_remaining_seats_travel(travels, max_seats_train, classes):
    # put train max seats in seat of travel 
    for j, rowcls in classes.iterrows():
        for i, row in travels.iterrows():
            nbseats = max_seats_train[(max_seats_train['class_id'] == rowcls['class_id']) & (max_seats_train['train_id'] == row['train_id'])]['nb_seats'].values[0]
            yield {"travel_id": row['travel_id'], "class_id": rowcls['class_id'], "nb_seats": nbseats}

def gen_max_seats_train(trains, classes):
    for j, rowcls in classes.iterrows():
        for i, row in trains.iterrows():
            yield {"train_id": row['train_id'], "class_id": rowcls['class_id'], "nb_seats": random.randint(10, 100)}

trains = pd.DataFrame(gen_trains(80))
classes = pd.DataFrame(gen_classes())
stations = pd.DataFrame(gen_stations(15))
max_seats_train = pd.DataFrame(gen_max_seats_train(trains, classes))

travels = pd.DataFrame(gen_travels(10000, stations, trains))
remaining_seats_travel = pd.DataFrame(gen_remaining_seats_travel(travels, max_seats_train, classes))

trains.columns

# Generate SQL Insertions
f = open("insert.sql", "w")
f.write("INSERT INTO trains (train_id, train_code, release_date) VALUES\n")
for i, row in trains.iterrows():
    f.write(f"({row['train_id']}, '{row['train_code']}', '{row['release_date']}'),\n")

f.write("INSERT INTO classes (class_id, name) VALUES\n")
for i, row in classes.iterrows():
    f.write(f"({row['class_id']}, '{row['name']}'),\n")

f.write("INSERT INTO stations (station_id, city, name) VALUES\n")
for i, row in stations.iterrows():
    f.write(f"({row['station_id']}, '{row['city']}', '{row['name']}'),\n")

f.write("INSERT INTO travels (travel_id, departure_date, arrival_date, from_station_id, to_station_id, train_id) VALUES\n")
for i, row in travels.iterrows():
    f.write(f"({row['travel_id']}, '{row['departure_date']}', '{row['arrival_date']}', {row['from_station_id']}, {row['to_station_id']}, {row['train_id']}),\n")

f.write("INSERT INTO remaining_seats_travel (travel_id, class_id, nb_seats) VALUES\n")

for i, row in remaining_seats_travel.iterrows():
    f.write(f"({row['travel_id']}, {row['class_id']}, {row['nb_seats']}),\n")

f.write("INSERT INTO max_seats_train (train_id, class_id, nb_seats) VALUES\n")
for i, row in max_seats_train.iterrows():
    f.write(f"({row['train_id']}, {row['class_id']}, {row['nb_seats']}),\n")

f.close()





