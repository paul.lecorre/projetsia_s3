package client.views;

import javax.swing.*;
import javax.swing.border.EmptyBorder;
import client.controlers.ConnexionListener;
import client.controlers.InscriptionListener;

import java.awt.*;


public class LoginRegister extends JFrame {

    public LoginRegister() {
        setTitle("Connexion");
        setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        setSize(600, 400);
        getContentPane().setBackground(Color.LIGHT_GRAY);

        // Panneau principal
        JPanel mainPanel = new JPanel();
        mainPanel.setLayout(new BoxLayout(mainPanel, BoxLayout.X_AXIS));
        mainPanel.setBorder(new EmptyBorder(new Insets(20, 20, 20, 20)));

        // Panneau d'inscription
        JPanel inscriptionPanel = new JPanel();
        inscriptionPanel.setLayout(new GridLayout(0, 1, 10, 10));
        inscriptionPanel.setBorder(BorderFactory.createTitledBorder("Inscription"));
        inscriptionPanel.setBackground(Color.WHITE);

        // Champs d'inscription
        inscriptionPanel.add(new JLabel("Nom d'utilisateur:"));
        JTextField usernameRegister = new PlaceholderTextField("Entrez votre nom d'utilisateur");
        usernameRegister.setForeground(Color.GRAY);
        usernameRegister.setMaximumSize(new Dimension(Integer.MAX_VALUE, usernameRegister.getPreferredSize().height));
        inscriptionPanel.add(usernameRegister);

        inscriptionPanel.add(new JLabel("Mot de passe:"));
        JPasswordField passwordRegister = new JPasswordField();
        inscriptionPanel.add(passwordRegister);

        JButton btnInscription = new RoundedButton("Inscrire");
        btnInscription.setBackground(Color.GREEN);
        btnInscription.setForeground(Color.WHITE);
        inscriptionPanel.add(btnInscription);

        // Panneau de connexion
        JPanel connexionPanel = new JPanel();
        connexionPanel.setLayout(new GridLayout(0, 1, 10, 10));
        connexionPanel.setBorder(BorderFactory.createTitledBorder("Connexion"));
        connexionPanel.setBackground(Color.WHITE);

        // Champs de connexion
        connexionPanel.add(new JLabel("Nom d'utilisateur:"));
        JTextField usernameLogin = new PlaceholderTextField("Entrez votre nom d'utilisateur");
        usernameLogin.setForeground(Color.GRAY);
        connexionPanel.add(usernameLogin);

        connexionPanel.add(new JLabel("Mot de passe:"));
        JPasswordField passwordLogin = new JPasswordField();
        connexionPanel.add(passwordLogin);

        JButton btnConnexion = new RoundedButton("Connecter");
        btnConnexion.setBackground(Color.BLUE);
        btnConnexion.setForeground(Color.WHITE);
        connexionPanel.add(btnConnexion);

        // Ajout des panneaux au panneau principal
        mainPanel.add(inscriptionPanel);
        mainPanel.add(connexionPanel);
        add(mainPanel);

        // Gestionnaires d'événements pour le bouton de connexion
        btnConnexion.addActionListener(new ConnexionListener(usernameLogin,passwordLogin,this));
        btnInscription.addActionListener(new InscriptionListener(usernameRegister,passwordRegister,this));
    }
}

