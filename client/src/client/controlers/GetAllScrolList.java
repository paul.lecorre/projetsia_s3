package client.controlers;

import java.util.Arrays;
import java.util.LinkedList;

import soap.trains.spyne.BookingServiceStub;
import soap.trains.spyne.BookingServiceStub.*;

public class GetAllScrolList {

    public static String[] getStations(String token) {
        LinkedList<String> stations = new LinkedList<String>();
        try {
            BookingServiceStub stub = null;
            try {
                stub = new BookingServiceStub();
            } catch (org.apache.axis2.AxisFault e) {
                System.err.println(e);
            }
            GetAllStations params = new GetAllStations();
            params.setToken(token);
            GetAllStationsE params2 = new GetAllStationsE();
            params2.setGetAllStations(params);
            GetAllStationsResponseE resp = stub.getAllStations(params2);
            StationArray allStations = resp.getGetAllStationsResponse().getGetAllStationsResult();
            for (Station station : allStations.getStation()) {
                stations.add(station.getName());
            }

        } catch (Exception e) {
            System.err.println(e);
        }
        Object[] objArray = stations.toArray();
        return Arrays.copyOf(objArray, objArray.length, String[].class);
    }

    public static String[] getClasses(String token) {
        LinkedList<String> classes = new LinkedList<String>();
        try {
            BookingServiceStub stub = null;
            try {
                stub = new BookingServiceStub();
            } catch (org.apache.axis2.AxisFault e) {
                System.err.println(e);
            }
            GetAllClasses params = new GetAllClasses();
            params.setToken(token);
            GetAllClassesE params2 = new GetAllClassesE();
            params2.setGetAllClasses(params);
            GetAllClassesResponseE resp = stub.getAllClasses(params2);
            StringArray allClasses = resp.getGetAllClassesResponse().getGetAllClassesResult();
            for (String classe : allClasses.getString()) {
                classes.add(classe);
            }

        } catch (Exception e) {
            System.err.println(e);
        }
        Object[] objArray = classes.toArray();
        return Arrays.copyOf(objArray, objArray.length, String[].class);
    }

}
