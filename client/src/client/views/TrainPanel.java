package client.views;

import java.awt.BorderLayout;

import javax.swing.*;
import javax.swing.table.DefaultTableModel;

import client.controlers.ReservationListener;

public class TrainPanel extends JPanel {

    public TrainPanel(Object[][] data, String token, JTextField nbSeat, JComboBox classe) {
        this.setLayout(new BorderLayout());
        // Crée les données du tableau

        // Crée les titres des colonnes
        String[] columnNames = { "Numero de trajet","Train ID", "Heure de départ", "Heure d'arrivée", "Compagnie", "Sieges restant"};

        // Crée un modèle de tableau avec des cases à cocher
        DefaultTableModel model = new DefaultTableModel(data, columnNames);

        // Crée le tableau avec le modèle de données
        JTable trainTable = new JTable(model);

        trainTable.setRowSelectionAllowed(true);
        // Ajoute le tableau à un JScrollPane pour la gestion automatique du défilement
        JScrollPane scrollPane = new JScrollPane(trainTable);

        // Ajoute le JScrollPane à ce panel (TrainPanel)
        this.add(scrollPane, BorderLayout.NORTH);

        JButton reserver = new JButton("reserver");
        reserver.addActionListener(new ReservationListener(trainTable, token, classe, nbSeat));
        this.add(reserver, BorderLayout.SOUTH);
    }

    public TrainPanel(Object[][] dataAlle, Object[][] dataRetour, String token, JTextField nbSeat, JComboBox classe) {
        this.setLayout(new BorderLayout());
        JPanel north = new JPanel();

        // Crée les titres des colonnes
        String[] columnNames = { "Numero de trajet","Train ID", "Heure de départ", "Heure d'arrivée", "Compagnie", "Sieges restant"};

        // Crée un modèle de tableau avec des cases à cocher
        DefaultTableModel modelLeft = new DefaultTableModel(dataAlle, columnNames);

        // Crée le tableau avec le modèle de données
        JTable trainTableLeft = new JTable(modelLeft);

        trainTableLeft.setRowSelectionAllowed(true);
        // Ajoute le tableau à un JScrollPane pour la gestion automatique du défilement
        JScrollPane scrollPaneLeft = new JScrollPane(trainTableLeft);

        DefaultTableModel modelRight = new DefaultTableModel(dataRetour, columnNames);
        JTable trainTableRight = new JTable(modelRight);

        trainTableRight.setRowSelectionAllowed(true);
        // Ajoute le tableau à un JScrollPane pour la gestion automatique du défilement
        JScrollPane scrollPaneRight = new JScrollPane(trainTableRight);

        scrollPaneLeft.setBorder(BorderFactory.createTitledBorder("Allé"));
        scrollPaneRight.setBorder(BorderFactory.createTitledBorder("Retour"));

        // Ajoute le JScrollPane à ce panel (TrainPanel)
        north.add(scrollPaneLeft, BorderLayout.EAST);
        north.add(scrollPaneRight, BorderLayout.WEST);
        this.add(north, BorderLayout.NORTH);

        JButton reserver = new JButton("reserver");
        reserver.addActionListener(new ReservationListener(trainTableLeft, trainTableRight, token, classe, nbSeat));
        this.add(reserver, BorderLayout.SOUTH);
    }

    public TrainPanel() {
    }
}
